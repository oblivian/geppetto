# geppetto

A web app / cli for running benchmarks

## License
This project is licensed under the Apache 2.0 license.

## How the server works

We spin up three main threads:
* the http thread (http.Server)
* the benchmark worker thread (worker.Worker)
* the live-state collector for benchmarks (worker.WorkerStatus)

We have a channel web -> worker to submit benchmarks to run,
and another channel worker -> status to report on completion/failure
to the status collector.