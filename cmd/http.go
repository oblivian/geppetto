/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"log"
	"os"
	"os/signal"
	"path"
	"syscall"
	"time"

	nethttp "net/http"

	"gitlab.wikimedia.org/oblivian/geppetto/http"
	"gitlab.wikimedia.org/oblivian/geppetto/worker"

	"github.com/spf13/cobra"
)

// httpCmd represents the http command
var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Geppetto web interface",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := log.New(os.Stderr, "[geppetto] ", log.LstdFlags)
		debug, err := cmd.Flags().GetBool("debug")
		if err != nil {
			panic(err)
		}
		cwd, _ := os.Getwd()
		// Set up the benchmark executor and the web server
		wrk, err := worker.NewWorker(path.Join(cwd, "data"), logger)
		if err != nil {
			panic(err)
		}
		workerStatus := worker.GetWorkerStatus(wrk)
		workerStatus.Listen()
		http.Setup(workerStatus, logger, wrk.Queue())
		web := http.Server("127.0.0.1:8000", "./templates", debug)
		// Set up the basic signal handling
		done := make(chan os.Signal, 1)
		signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
		// Start the worker listener
		go wrk.Listen()
		go func() {
			logger.Printf("Listening on %s\n", web.Addr)
			if err := web.ListenAndServe(); err != nil && err != nethttp.ErrServerClosed {
				log.Fatalf("Error running the web server: %v\n", err)
			}
		}()
		// Signal handling!
		<-done
		logger.Println("Shutting down benchmarks...")
		wrk.Stop()
		workerStatus.Stop()
		logger.Println("Shutting down web server...")
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer func() {
			cancel()
		}()
		if err := web.Shutdown(ctx); err != nil {
			log.Fatalf("Server shutdown failed: %v\n", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(httpCmd)
	httpCmd.Flags().BoolP("debug", "d", false, "Debug mode - will e.g. reload templates on every request.")
}
