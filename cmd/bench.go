/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/oblivian/geppetto/worker"
)

var suite string
var savedir string
var save bool

// benchCmd represents the bench command
var benchCmd = &cobra.Command{
	Use:   "bench",
	Short: "Run a benchmark",
	Long:  `Launch a benchmarking session, based on a json configuration.`,
	Run: func(cmd *cobra.Command, args []string) {
		worker.Basedir = savedir
		s, err := worker.SuiteFromFile(suite)
		if err != nil {
			panic(err)
		}
		all_sets, err := s.GetBenchmarkSets()
		if err != nil {
			panic(err)
		}
		for name, set := range all_sets {
			fmt.Println("###########")
			fmt.Printf("Now running benchmarks for %s\n", name)
			if len(set.Results) != 0 {
				fmt.Println("Results already found.")
			} else {
				fmt.Printf("Connecting to %s\n", set.Fqdn)
				err := set.Run()
				if err != nil {
					panic(err)
				}
				if save {
					err = set.Save(name)
				}
				if err != nil {
					panic(err)
				}
			}
			for conc, results := range set.Results {
				fmt.Println("==")
				fmt.Printf("Run at concurrency %d\n", conc)
				fmt.Printf("Number of errors: %d\n", results.Errors)
				nresults := len(results.Timings)
				fmt.Printf("Number of successful requests: %d\n", nresults)
				sort.Float64s(results.Timings)
				fmt.Printf("p50: %d ms\n", getPerc(&results.Timings, nresults, 50))
				fmt.Printf("p90: %d ms\n", getPerc(&results.Timings, nresults, 90))
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(benchCmd)
	benchCmd.Flags().StringVarP(&suite, "suite-config", "s", "run.json", "A file describing the tests to run.")
	benchCmd.Flags().StringVarP(&savedir, "save-path", "p", "", "Path where to save records.")
	benchCmd.Flags().BoolVarP(&save, "save", "S", false, "Save records?.")

}

func getPerc(times *[]float64, num_elements int, percentile int) int {
	element := 0
	if percentile >= 100 {
		element = num_elements - 1
	} else if percentile > 0 {
		element = (num_elements*percentile)/100 - 1
	}
	if element < 0 {
		element = 0
	}
	return int((*times)[element])
}
