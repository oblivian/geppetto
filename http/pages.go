package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"sort"

	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/oblivian/geppetto/worker"
)

// Homepage handling
func homepage(w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadDir(worker.Basedir)
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Cannot access the working directory.", http.StatusInternalServerError)
		return
	}
	all_suites, err := worker.GetAllSuites()
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error, please refer to the server logs for details", http.StatusInternalServerError)
	}
	err = templates.Render(w, "index.html.gotmpl", all_suites)
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error, please refer to the server logs for details", http.StatusInternalServerError)
	}
}

func suite(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	// TODO: apply path.Clean to the parameter? It needs validation
	// anyways
	suite, err := worker.SuiteFromName(params["suite"])
	if err != nil {
		logger.Printf("%v\n", err)
		// TODO: diversify status code depending on if the
		// error is non existence of the suite or an invalid value.
		http.Error(w, "Could not find this suite", http.StatusNotFound)
		return
	}

	if err = renderSuite(suite, w); err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

func renderSuite(suite *worker.BenchmarkSuite, w http.ResponseWriter) error {
	sets, err := suite.GetBenchmarkSets()
	if err != nil {
		return err
	}

	var allData struct {
		Suite  *worker.BenchmarkSuite
		Status map[string]int
	}
	allData.Suite = suite
	allData.Status = make(map[string]int, len(sets))
	for name, set := range sets {
		allData.Status[name] = set.Progress()
	}
	return templates.Render(w, "suite.html.gotmpl", allData)
}

// handler for GET requests to /suite/:suite/{edit,new}
func editSuite(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var data []byte
	var err error
	var suite *worker.BenchmarkSuite
	if params["action"] == "edit" {
		suite, err = worker.SuiteFromName(params["suite"])
		if err != nil {
			http.Error(w, "suite not found", http.StatusNotFound)
			return
		}
	} else if params["action"] == "new" {
		suite = &worker.BenchmarkSuite{
			Name: params["suite"],
		}
	}
	data, err = json.MarshalIndent(suite, "", " ")
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	var toRender struct {
		Name string
		Data string
	}
	toRender.Name = suite.Name
	toRender.Data = string(data)
	err = templates.Render(w, "editsuite.html.gotmpl", toRender)
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func saveSuite(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	data := []byte(r.FormValue("data"))
	suite_name := params["suite"]
	suite, err := worker.SuiteFromData(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("Error in data validation: %v", err), http.StatusBadRequest)
		return
	}
	// We need to check if the suite name was changed. In that case,
	// we'll need to redirect to the correct page afterwards.
	if suite.Name != suite_name {
		http.Error(w, "Cannot change the name of the suite", http.StatusBadRequest)
		return
	}
	if err = suite.Save(); err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Could not save the suite", http.StatusInternalServerError)
		return
	}
	if err = renderSuite(suite, w); err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

// Handler for GET requests to /graphs/:suite/:graph
func graph(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Not implemented yet", http.StatusNotImplemented)
}

// Handler for POST/GET requests to /graphs/:suite
func gengraph(w http.ResponseWriter, r *http.Request) {
	dataSetColors := []string{"green", "blue", "violet", "orange", "yellow"}
	percentiles := []int{50, 75, 90, 95, 99}
	params := mux.Vars(r)
	suite, err := worker.SuiteFromName(params["suite"])
	if err != nil {
		logger.Printf("%v\n", err)
		// TODO: diversify status code depending on if the
		// error is non existence of the suite or an invalid value.
		http.Error(w, "Could not find this suite", http.StatusNotFound)
		return
	}
	sets, err := suite.GetBenchmarkSets()
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server Error", http.StatusInternalServerError)
		return
	}
	r.ParseForm()
	request_sets := r.Form["set"]
	percDatasets := make(map[int][]ChartjsDataset)
	fullDatasets := make(map[int][]ChartjsDataset)
	numRequests := 0
	for idx, name := range request_sets {
		set, ok := sets[name]
		if set.NumRequests > numRequests {
			numRequests = set.NumRequests
		}
		if !ok {
			logger.Printf("Bad request post")
			http.Error(w, fmt.Sprintf("benchmark set %s not found", name), http.StatusBadRequest)
			return
		}
		for concurrency, results := range set.Results {
			numData := len(results.Timings)
			sort.Float64s(results.Timings)
			fullDatasets[concurrency] = append(fullDatasets[concurrency],
				ChartjsDataset{
					Label:           path.Base(path.Dir(set.Id)),
					BackGroundColor: dataSetColors[idx],
					BorderColor:     dataSetColors[idx],
					BorderWidth:     2,
					Data:            results.Timings,
				},
			)
			// Percentiles
			percData := make([]float64, 5)
			for id, perc := range percentiles {
				percData[id] = getPerc(&results.Timings, numData, perc)
			}
			d := ChartjsDataset{
				Label:           path.Base(path.Dir(set.Id)),
				BackGroundColor: dataSetColors[idx],
				BorderColor:     dataSetColors[idx],
				BorderWidth:     0,
				Data:            percData,
			}
			percDatasets[concurrency] = append(percDatasets[concurrency], d)
		}
	}
	var toRender struct {
		Concurrencies []int
		Timings       map[int][]ChartjsDataset
		Percentiles   map[int][]ChartjsDataset
	}
	toRender.Concurrencies = make([]int, 0, len(percDatasets))
	for k := range percDatasets {
		toRender.Concurrencies = append(toRender.Concurrencies, k)
	}
	sort.Ints(toRender.Concurrencies)
	toRender.Timings = fullDatasets
	toRender.Percentiles = percDatasets
	err = templates.Render(w, "graphs.html.gotmpl", toRender)
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server Error", http.StatusInternalServerError)
		return
	}
}

// Handler for GET requests to /run/{suite}/{config}/{test}
func runbenchmarkset(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	logger.Printf("params: suite: %s, config: %s, test: %s\n", params["suite"], params["config"], params["test"])
	slug := path.Join(params["suite"], params["config"], params["test"])
	queue <- slug
	err := templates.Render(w, "job_submitted.html.gotmpl", workerStatus)
	if err != nil {
		logger.Printf("%v\n", err)
		http.Error(w, "Internal server Error", http.StatusInternalServerError)
		return
	}
}

func getPerc(times *[]float64, num_elements int, percentile int) float64 {
	element := 0
	if percentile >= 100 {
		element = num_elements - 1
	} else if percentile > 0 {
		element = (num_elements*percentile)/100 - 1
	}
	if element < 0 {
		element = 0
	}
	return (*times)[element]
}

type ChartjsDataset struct {
	Label           string    `json:"label"`
	Data            []float64 `json:"data"`
	BackGroundColor string    `json:"backgroundColor"`
	BorderColor     string    `json:"borderColor"`
	BorderWidth     int       `json:"borderWidth"`
}
