package http

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path"

	"github.com/Masterminds/sprig/v3"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.wikimedia.org/oblivian/geppetto/worker"
)

/*
Copyright © 2022 Giuseppe Lavagetto

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/*
Middleware for telemetry
*/
var httpDuration = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Name: "geppetto_http_duration_seconds",
		Help: "Duration of http requests",
	},
	[]string{"path"},
)

func telemetryMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			route := mux.CurrentRoute(r)
			path, _ := route.GetPathTemplate()
			timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
			next.ServeHTTP(w, r)
			timer.ObserveDuration()
		},
	)
}

func addMiddleware(r *mux.Router) {
	r.Use(telemetryMiddleware)
	r.Path("/metrics").Handler(promhttp.Handler())
}

/*
 Templates
*/
type TemplateLoader struct {
	Glob     string
	Debug    bool
	template *template.Template
}

func (tl *TemplateLoader) Render(w http.ResponseWriter, name string, data interface{}) error {
	if tl.Debug || tl.template == nil {
		tl.template = template.Must(template.New("base").Funcs(sprig.FuncMap()).ParseGlob(tl.Glob))
	}
	return tl.template.ExecuteTemplate(w, name, data)
}

func static(r *mux.Router, path string, docroot string) {
	dir := http.FileServer(http.Dir(docroot))
	r.PathPrefix(path).Handler(http.StripPrefix(path, dir))
}

var templates *TemplateLoader

/*
 The server
*/
var logger *log.Logger
var workerStatus *worker.WorkerStatus

func Setup(w *worker.WorkerStatus, l *log.Logger, q chan string) {
	logger = l
	setStatus(w)
	setQueue(q)
}

func setStatus(w *worker.WorkerStatus) {
	workerStatus = w
}

var queue chan string

func setQueue(q chan string) {
	queue = q
}

func Server(addr string, templates_path string, debug bool) *http.Server {
	router := mux.NewRouter()
	addMiddleware(router)
	http.Handle("/", router)
	templates = &TemplateLoader{
		Glob:  path.Join(templates_path, "*.html.gotmpl"),
		Debug: debug,
	}
	// Add routes here.
	static(router, "/static", path.Join(templates_path, "static"))
	// Homepage
	router.HandleFunc("/", homepage)
	// Show a suite (GET), Save suite (POST)
	router.HandleFunc("/suite/{suite}", suite).Methods("GET")
	router.HandleFunc("/suite/{suite}", saveSuite).Methods("POST")
	// Allow modifying/creating a suite
	router.HandleFunc("/suite/{suite}/{action}", editSuite)
	// Show a set of graphs generated for the suite
	router.HandleFunc("/graph/{suite}/{graph}", graph)
	// Generate a new graph for the suite (POST)
	router.HandleFunc("/graph/{suite}", gengraph)
	// Run a benchmark set (POST) or get status of a current run (GET)
	router.HandleFunc("/run/{suite}/{config}/{test}", runbenchmarkset)
	customLog := handlers.CombinedLoggingHandler(os.Stdout, http.DefaultServeMux)
	server := &http.Server{Addr: addr, Handler: customLog}
	return server
}
