package worker

import "github.com/Arafatk/glot"

// PlotRequest represents a plot request
type PlotRequest struct {
	// Name of the plot
	Name string
	// If the plot is a percentiles histogram or linear
	Percentile bool
	// Map of plottable names and benchmark results
	Benchmarks map[string]*BenchmarkResults
}

// Plot generates a plot from a plot request,
// returns the path on disk of the image.
func (p *PlotRequest) Plot(destination string) (string, error) {
	plot, err := glot.NewPlot(1, false, false)
	if err != nil {
		return "", err
	}
	if p.Percentile {
		return p.PlotPercentile(plot, destination)
	} else {
		return p.PlotLines(plot, destination)
	}
}

func (p *PlotRequest) PlotPercentile(plot *glot.Plot, destination string) (string, error) {
	plot.SetTitle("Histogram comparison")
	return "", nil
}

func (p *PlotRequest) PlotLines(plot *glot.Plot, destination string) (string, error) {
	plot.SetTitle("Linear comparison")
	return "", nil
}
