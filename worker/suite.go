package worker

import (
	"encoding/json"
	"io/ioutil"
	"path"
)

// A benchmark suite identifies
// a full group of urls to call,
// and contains the various benchmark sets
type BenchmarkSuite struct {
	// Name of the whole benchmark suite. It can be e.g. a task number.
	Name string `json:"name"`
	// map of name -> (host, headers)
	// for the various benchmarks to run.
	// For example names could be "php7", "new_host", etc...
	Configs map[string]*SuiteConfig `json:"configs"`
	// Set of tests to run for each config.
	Tests map[string]*SuiteTest `json:"tests"`
}

type SuiteConfig struct {
	// the fqdn to connect to
	Fqdn string `json:"host"`
	// Additional headers for this specific test
	Headers map[string]string `json:"headers"`
}

type SuiteTest struct {
	// The Url to call
	Url string `json:"url"`
	// Number of requests to perform
	NumRequests int `json:"nreq"`
	// Min concurrency to test
	MinConcurrency int `json:"min_concurrency"`
	// Max concurrency
	MaxConcurrency int `json:"max_concurrency"`
	// Step for concurrency
	Step int `json:"step"`
}

// Generate all the benchmark sets from our suite.
func (b *BenchmarkSuite) GetBenchmarkSets() (map[string]*BenchmarkSet, error) {
	all := make(map[string]*BenchmarkSet, 0)
	for conf_name, conf := range b.Configs {
		for test_name, test := range b.Tests {
			slug := path.Join(b.Name, conf_name, test_name)
			// First try to load from disk. If not present, just create the new set.
			set, err := NewSetFromDisk(slug)
			if err != nil {
				set, err = NewBenchmarkSet(
					slug,
					conf.Fqdn,
					test.Url,
					test.NumRequests, conf.Headers, test.MinConcurrency, test.MaxConcurrency, test.Step)
			}
			if err != nil {
				return nil, err
			}
			all[slug] = set
		}
	}
	return all, nil
}

func (b *BenchmarkSuite) Save() error {
	record, err := NewBenchmarkRecord(b.Name, "suite")
	if err != nil {
		return err
	}
	data, err := json.MarshalIndent(b, "", " ")
	if err != nil {
		return err
	}
	return record.SaveData(&data)
}

func SuiteFromData(data *[]byte) (*BenchmarkSuite, error) {
	var suite BenchmarkSuite
	err := json.Unmarshal(*data, &suite)
	return &suite, err
}

func SuiteFromName(name string) (*BenchmarkSuite, error) {
	record, err := NewBenchmarkRecord(name, "suite")
	if err != nil {
		return nil, err
	}
	content, err := record.FetchData()
	if err != nil {
		return nil, err
	}
	return SuiteFromData(content)
}

// Get all the Suites saved.
func GetAllSuites() ([]*BenchmarkSuite, error) {
	records, err := ListRecords("suite")
	suites := make([]*BenchmarkSuite, 0, len(records))
	if err != nil {
		return nil, err
	}
	for _, record := range records {
		suite, err := SuiteFromName(record)
		if err == nil {
			suites = append(suites, suite)
		}
		// TODO: log errors
	}
	return suites, nil
}

// To use to load a single suite.
func SuiteFromFile(filename string) (*BenchmarkSuite, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return SuiteFromData(&contents)
}
