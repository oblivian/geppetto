package worker

// This file contains package-level variables.

// Variable used to stop the benchmarking
var interrupted int32

// Channel for termination of benchmarking
var benchmarkStop chan int
