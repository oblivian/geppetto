package worker

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// TODO: support multiple storage backends.
// That would be accomplished by
// a) making benchmarkrecord an interface
// b) making it work without files.

// The base directory for storage
var Basedir string

func IsDir(path string) bool {
	finfo, err := os.Stat(path)
	return (!os.IsNotExist(err) && finfo.IsDir())
}

type BenchmarkRecord struct {
	Key  string
	Type string
}

func NewBenchmarkRecord(key string, kind string) (*BenchmarkRecord, error) {
	if !path.IsAbs(Basedir) {
		return nil, fmt.Errorf("%s is not an absolute path", Basedir)
	}
	if !IsDir(Basedir) {
		return nil, fmt.Errorf("base path %s is not a directory", Basedir)
	}
	r := &BenchmarkRecord{Key: key, Type: kind}
	p := path.Dir(r.filename())
	if !IsDir(p) {
		if err := os.MkdirAll(p, 0755); err != nil {
			return nil, err
		}
	}
	return r, nil
}

func (r *BenchmarkRecord) filename() string {
	return path.Join(Basedir, r.Type, fmt.Sprintf("%s.json", r.Key))
}

// Exists tells us if the record exists or not.
func (r BenchmarkRecord) Exists() bool {
	_, err := os.Stat(r.filename())
	return !os.IsNotExist(err)
}

func (r *BenchmarkRecord) FetchData() (*[]byte, error) {
	if !r.Exists() {
		return nil, fmt.Errorf("the record doesn't exist")
	}
	content, err := ioutil.ReadFile(r.filename())
	return &content, err
}

func (r *BenchmarkRecord) SaveData(content *[]byte) error {
	return ioutil.WriteFile(r.filename(), *content, 0644)
}

func ListRecords(kind string) ([]string, error) {
	var records []string
	err := filepath.Walk(
		path.Join(Basedir, kind),
		func(p string, info fs.FileInfo, err error) error {
			if path.Ext(p) == ".json" {
				name := strings.Replace(p, path.Join(Basedir, kind)+"/", "", 1)
				name = strings.Replace(name, ".json", "", 1)
				records = append(records, name)
			}
			return nil
		},
	)
	return records, err
}
