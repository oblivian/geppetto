package worker

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync/atomic"
	"time"

	"github.com/lavagetto/go-wrk/loader"
	"github.com/lavagetto/go-wrk/util"
)

func StopBenchmark() {
	atomic.StoreInt32(&interrupted, 1)
	benchmarkStop <- 1
}

// A benchmarkset is identified by:
// An Id
// A url to call
// An actual fqdn to connect to
// A number of requests to perform
// Min/max concurrency to use
// Headers to add
type BenchmarkSet struct {
	// The label of this benchmark
	Id string
	// The fqdn of the server we are connecting to
	Fqdn string
	// The full url to request.
	Url string
	// Total number of requests
	NumRequests int
	// k-v list of additional Headers
	Headers map[string]string
	// Minimum concurrency
	MinConcurrency int
	// Maximum concurrency
	MaxConcurrency int
	// Concurrency step
	Step int
	// Results separated by concurrency
	Results map[int]*BenchmarkResults
}

func NewBenchmarkSet(id string, fqdn string, url string, n int, hdr map[string]string, min int, max int, incr int) (*BenchmarkSet, error) {
	if min >= max {
		return nil, fmt.Errorf("minimum concurrency must be smaller than the maximum one")
	}
	results := make(map[int]*BenchmarkResults, incr)
	return &BenchmarkSet{id, fqdn, url, n, hdr, min, max, incr, results}, nil
}

// Runs sequentially the benchmarks at all concurrencies.
func (bs *BenchmarkSet) Run() error {
	loader.USER_AGENT = "Geppetto/0.1 WMF"
	request, err := NewBenchmarkRequest(bs.Fqdn, bs.Url, &bs.Headers)
	if err != nil {
		return err
	}
	req := make(chan int)
	res := make(chan *BenchmarkResponse)
	for routines := bs.MinConcurrency; routines <= bs.MaxConcurrency && atomic.LoadInt32(&interrupted) == 0; routines += bs.Step {
		b := Benchmark{routines, bs.NumRequests, req, res, make(chan int)}
		go func() {
			for i := 0; i < bs.NumRequests; i++ {
				req <- 1
			}
			for i := 0; i < routines; i++ {
				req <- 0
			}
		}()
		// This is blocking, by design. We want our desired concurrency to the hosts,
		// not a sum of our desired concurrencies.
		bs.Results[routines], err = b.Run(request)
		if err != nil {
			return err
		}
	}
	return nil
}

// Report a percentage of completion of the benchmark set run.
func (bs *BenchmarkSet) Progress() int {
	var p float64
	// no results -> zero progress
	if len(bs.Results) == 0 {
		return 0
	}
	var all float64
	for c := bs.MinConcurrency; c <= bs.MaxConcurrency; c += bs.Step {
		all += 1.0
		if result, ok := bs.Results[c]; ok {
			p += float64(len(result.Timings)+result.Errors) / float64(bs.NumRequests)
		}
	}
	if all == 0.0 {
		return 0
	}
	return int((p / all) * 100.0)
}

func NewSetFromDisk(key string) (*BenchmarkSet, error) {
	b, err := NewBenchmarkRecord(key, "set")
	if err != nil {
		return nil, err
	}
	content, err := b.FetchData()
	if err != nil {
		return nil, err
	}
	var set BenchmarkSet
	err = json.Unmarshal(*content, &set)
	return &set, err
}

// Save the benchmarkset at the specific value.
func (bs *BenchmarkSet) Save(key string) error {
	b, err := NewBenchmarkRecord(key, "set")
	if err != nil {
		return err
	}
	data, err := json.MarshalIndent(bs, "", " ")
	if err != nil {
		return err
	}
	return b.SaveData(&data)
}

// BenchmarkRequest describes a request
type BenchmarkRequest struct {
	Host    string
	Url     string
	Headers *map[string]string
}

// Make a request for a url connecting to a specific host
func NewBenchmarkRequest(fqdn string, uri string, headers *map[string]string) (*BenchmarkRequest, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}
	host := u.Host
	if strings.Contains(host, ":") {
		host, _, err = net.SplitHostPort(u.Host)
		if err != nil {
			return nil, err
		}
	}
	Url := strings.Replace(uri, host, fqdn, 1)
	return &BenchmarkRequest{host, Url, headers}, nil
}

// Benchmark is a single benchmark to run. Basically the Benchmarkset, but at a specific concurrency
type Benchmark struct {
	// Number of routines
	Routines int
	// Number of requests per routine
	NumRequests int
	// Channel for incoming requests
	requests chan int
	// Channel to get results
	results chan *BenchmarkResponse
	// Control channel for request threads
	terminated chan int
}

// Run spawns N goroutines (the same number as the required concurrency)
// Then runs M requests per goroutine so that M*N = NumRequests
func (b *Benchmark) Run(r *BenchmarkRequest) (*BenchmarkResults, error) {
	results := BenchmarkResults{make([]float64, 0, b.NumRequests), 0}
	for i := 0; i < b.Routines; i++ {
		go b.requestThread(r)
	}
	running := b.Routines
	for running > 0 {
		select {
		case <-b.terminated:
			running--
		case result := <-b.results:
			if result.error {
				results.Errors++
			} else {
				results.Timings = append(results.Timings, result.duration)
			}
		}
	}
	return &results, nil
}

// Makes requests repeatedly and sends the results over a channel
func (b *Benchmark) requestThread(r *BenchmarkRequest) {
	// TODO: have the timeout for each request be a parameter.
	client := httpClient(15000)
	var running int
	for {
		running = <-b.requests
		if running == 0 || atomic.LoadInt32(&interrupted) != 0 {
			b.terminated <- 1
			return
		}
		var resp BenchmarkResponse
		size, duration := loader.DoRequest(client, *r.Headers, "GET", r.Host, r.Url, "")
		time := float64(duration.Milliseconds())
		if size > 0 {
			resp = BenchmarkResponse{size, time, false}
		} else {
			resp = BenchmarkResponse{size, time, true}
		}
		b.results <- &resp
	}
}

type BenchmarkResponse struct {
	respSize int
	duration float64
	error    bool
}

// BenchmarkResult is the result of the benchmark
type BenchmarkResults struct {
	// Response times in milliseconds
	Timings []float64
	// Errors
	Errors int
}

// BenchmarkProgress is a signal that can be sent over a channel to signal progress of the benchmark
type BenchmarkProgress struct {
	// The id of the benchmark
	Id string
	// Progress for each concurrency we're running with.
	Progress map[int32]int32
}

// http calls. We piggyback on the work done by go-wrk
// but we need our special sauce.
func httpClient(timeoutms int) *http.Client {
	client := &http.Client{}
	client.Transport = &http.Transport{
		ResponseHeaderTimeout: time.Millisecond * time.Duration(timeoutms),
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	}
	// We don't allow redirects.
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return util.NewRedirectError("redirection not allowed")
	}
	return client
}
