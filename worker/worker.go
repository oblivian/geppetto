package worker

import (
	"fmt"
	"log"
	"path"
	"sync"
	"sync/atomic"
)

type StatusCode int

type WorkerStatusUpdate struct {
	Slug   string
	Status StatusCode
}

const (
	NOT_FOUND StatusCode = iota
	ERROR
	RUNNING
	COMPLETED
)

// Worker is the controller of the whole benchmarking process.
// It receives requests for running benchmark sets via its queue,
// and initializes the corresponding BenchmarkSuites, and from there
// the corresponding BenchmarkSets (groups of benchmarks with the same
// configuration, at increasing concurrency), which in turn run its
// actual Benchmarks.
type Worker struct {
	// Where the worker listens for benchmark requests
	queue      chan string
	logger     *log.Logger
	replicator chan WorkerStatusUpdate
	stopper    chan int
}

func NewWorker(databasePath string, logger *log.Logger) (*Worker, error) {
	// initialize the stopper if not existent
	if benchmarkStop == nil {
		benchmarkStop = make(chan int)
	}
	if !path.IsAbs(databasePath) {
		return nil, fmt.Errorf("%s is not an absolute path", databasePath)
	}
	Basedir = path.Clean(databasePath)
	return &Worker{
		queue:      make(chan string),
		logger:     logger,
		replicator: make(chan WorkerStatusUpdate),
		stopper:    make(chan int),
	}, nil
}

func (w *Worker) Queue() chan string {
	return w.queue
}

func (w *Worker) SetStatus(slug string, value StatusCode) {
	w.replicator <- WorkerStatusUpdate{slug, value}
}

// Stops both the worker and the benchmarks it might have launched.
func (w *Worker) Stop() {
	// This is used to stop the benchmark loops.
	atomic.StoreInt32(&interrupted, 1)
	// Warning: this will block if there is no receiver.
	// So we need to stop runSet first (via the coordinator above)
	w.stopper <- 1
	w.logger.Printf("Stop signal sent from worker.")
}

// Reset stops all benchmarks running, then sets the worker back in a status
// where it can function.
func (w *Worker) Reset() {
	w.Stop()
	atomic.StoreInt32(&interrupted, 0)
}

func (w *Worker) Listen() {
	go func() {
		// We can't coordinate using interrupted here, else we'll end up
		// blocking forever on waiting on messages.
		for {
			select {
			case <-w.stopper:
				w.logger.Printf("Stop signal received by listener, shutting down...")
				return
			case set_slug := <-w.queue:
				w.runSet(set_slug)
			}
		}
	}()
}

func (w *Worker) runSet(set_slug string) {
	suite := path.Dir(path.Dir(set_slug))
	s, err := SuiteFromName(suite)
	if err != nil {
		w.logger.Printf("Could not initialize suite %s: %v", suite, err)
		w.SetStatus(set_slug, ERROR)
		return
	}
	sets, err := s.GetBenchmarkSets()
	if err != nil {
		w.logger.Printf("Could not get the benchmark sets for suite %s: %v", suite, err)
		w.SetStatus(set_slug, ERROR)
		return
	}
	set, ok := sets[set_slug]
	if !ok {
		w.logger.Printf("Could not find the required benchmark to run")
		w.SetStatus(set_slug, ERROR)
		return
	}
	w.SetStatus(set_slug, RUNNING)
	// TODO: this part is blocking.
	// If we want to be able to run multiple benchmark sets at a
	// time, we'll need to spin this out as a function and create a
	// thread pool.
	err = set.Run()
	// TODO: check if the set is complete *and* if interrupted has a value of 1.
	// In that case, return an error status.
	if err != nil {
		w.logger.Printf("Error running benchmarks %s: %v", set_slug, err)
		w.SetStatus(set_slug, ERROR)
		return
	}
	err = set.Save(set_slug)
	if err != nil {
		w.logger.Printf("Error saving benchmarks %s: %v", set_slug, err)
		w.SetStatus(set_slug, ERROR)
	} else {
		w.logger.Printf("Finished running benchmarks %s", set_slug)
		w.SetStatus(set_slug, COMPLETED)
	}
}

type WorkerStatus struct {
	replica chan WorkerStatusUpdate
	status  map[string]StatusCode
	mutex   sync.Mutex
	stopper chan int
}

func GetWorkerStatus(w *Worker) *WorkerStatus {
	return &WorkerStatus{
		replica: w.replicator,
		status:  make(map[string]StatusCode),
		mutex:   sync.Mutex{},
		stopper: make(chan int),
	}
}

func (ws *WorkerStatus) Listen() {
	go func() {
		for {
			select {
			case <-ws.stopper:
				return
			case update := <-ws.replica:
				ws.mutex.Lock()
				ws.status[update.Slug] = update.Status
				ws.mutex.Unlock()
			}
		}
	}()
}

func (ws *WorkerStatus) Stop() {
	ws.stopper <- 1
}

// Check status
func (w *WorkerStatus) Get(slug string) StatusCode {
	w.mutex.Lock()
	if st, ok := w.status[slug]; ok {
		return st
	}
	return NOT_FOUND
}

// Check if there is any ongoing work.
func (w *WorkerStatus) IsRunning() bool {
	w.mutex.Lock()
	defer w.mutex.Unlock()
	for _, val := range w.status {
		if val == RUNNING {
			return true
		}
	}
	return false
}
